﻿Out-File -filepath "\\store1\share\repo_report\whatsonrepo.csv"

$webrepo = Import-Csv -Path "\\store1\share\repo_report\repo.csv"       
foreach ($webrepo in $webrepo)
{
    $WebResponse = Invoke-WebRequest $webrepo.repo
    $gettxt = $WebResponse.Links | Select href

    $settext1 = $gettxt | Select-String -Pattern 'GoldApiServer' -CaseSensitive -SimpleMatch | select-object -first 1

    $Regex = [Regex]::new("(?<=x86_64.rpm</a>)(.*)(?=(?:[^x]|^)\d\d\d(?:[^ip]|$))")           
    $Match = $Regex.Match($WebResponse.RawContent)           
    if($Match.Success)           
    {           
        $settext2 = $Match.Value           
    }

    $settext2 = $settext2.trim()

    if (-not ($settext2.Length -eq 16))
    {
         $Regex = [Regex]::new("(\d\d\d\d\d\d\d\d\d\d\d\d)")           
            $Match = $Regex.Match($settext1)           
            if($Match.Success) 
            {
              $settext2 = [datetime]::parseexact($Match.Value, 'yyyyMMddHHmm', $null).ToString('yyyy-MM-dd HH:mm')  
            }    
    }
     
    $name = "$settext1" + " , "+ $settext2 +" , "+ $webrepo.reponame

    #$name

    $settext1 = ""
    $settext2 = ""

   add-content -path "\\store1\share\repo_report\whatsonrepo.csv" -value $name

}
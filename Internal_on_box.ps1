﻿Out-File -filepath "\\store1\share\repo_report\whatsonbox.csv"

add-content -path "\\store1\share\repo_report\whatsonbox.csv" -value "environment,api version, gold version, build date"

$webrepo = Import-Csv -Path "\\store1\share\repo_report\supboxes.csv"       
foreach ($webrepo in $webrepo)
{
    $WebRequest = Invoke-WebRequest  $webrepo.infopage

        $results = $WebRequest.RawContent

        #get API details
        $Regex = [Regex]::new("(?<=Version:</label>)(.*)(?=</td>)")           
        $Match = $Regex.Match($results.ToString())   
        if($Match.Success)           
        {           
            $apiversion = $Match.Value           
        }

        #get API build date
        $Regex = [Regex]::new("(?<=Date:</label>)(.*)(?=</td>)")           
        $Match = $Regex.Match($results.ToString())   
        if($Match.Success)           
        {           
            $apiBuilddate = $Match.Value           
        }

        #get Gold Version
        $Regex = [Regex]::new("(?<=Gold</strong></td>
			<td><label>Version:</label>)(.*)(?=</td>)")           
        $Match = $Regex.Match($results.ToString())   
        if($Match.Success)           
        {           
            $goldversion = $Match.Value           
        }

        $name = $webrepo.name +","+$apiversion.Trim() + "," + $goldversion.trim() + "," + $apiBuilddate.Trim()

       # $name

        $apiversion = ""
        $goldversion = ""
        $apiBuilddate = ""

       add-content -path "\\store1\share\repo_report\whatsonbox.csv" -value $name
}
﻿Out-File -filepath "\\store1\share\repo_report\whatsoncustbox.csv"

add-content -path "\\store1\share\repo_report\whatsoncustbox.csv" -value "customer,api version, gold version, build date"

$webrepo = Import-Csv -Path "\\store1\share\repo_report\customerboxes.csv"       
foreach ($webrepo in $webrepo)
{
    $error.clear()
    try { $WebRequest = Invoke-WebRequest $webrepo.info }   
    catch { $name = $webrepo.name+",unavailable 503,unavailable 503,00:00 01/01/1900" }
    if (!$error)
        {
            $results = $WebRequest.RawContent

            #$WebRequest.RawContent

            #get API details
            $Regex = [Regex]::new("(?<=Version:</label>)(.*)(?=</td>)")           
            $Match = $Regex.Match($results.ToString())   
            if($Match.Success)           
            {           
                $apiversion = $Match.Value           
            }
            else
                {
                    #get API details
                    $Regex = [Regex]::new("(?<=<td>Gold API Server Version</td>
            <td>)(.*)(?=</td>)")           
                    $Match = $Regex.Match($results.ToString())   
                    if($Match.Success) 
                                {           
                                    $apiversion = $Match.Value           
                                }
                }

            #get API build date
            $Regex = [Regex]::new("(?<=Date:</label>)(.*)(?=</td>)")           
            $Match = $Regex.Match($results.ToString())   
            if($Match.Success)           
            {           
                $apiBuilddate = $Match.Value           
            }

            #get Gold Version
            $Regex = [Regex]::new("(?<=Gold</strong></td>
			    <td><label>Version:</label>)(.*)(?=</td>)")           
            $Match = $Regex.Match($results.ToString())   
            if($Match.Success)           
            {           
                $goldversion = $Match.Value           
            }
            else
                {
                    $Regex = [Regex]::new("(?<=Gold Version</td>
            <td>)(.*)(?=</td>)")           
                    $Match = $Regex.Match($results.ToString())   
                    if($Match.Success)
                        {
                            $goldversion = $Match.Value
                        }
                    else 
                        {
                                $Regex = [Regex]::new("(?<=Gold</strong></td>
			<td><label>Version:</label>)(.*)(?=</td>)")           
                                $Match = $Regex.Match($results.ToString())   
                                if($Match.Success)
                                    {
                                        $goldversion = $Match.Value
                                    }
                        }
                }

             if (!$apiBuilddate) { $apiBuilddate = "00:00 01/01/1900" }

                    $name = $webrepo.name +","+$apiversion.Trim() + "," + $goldversion.trim() + "," + $apiBuilddate.Trim()
        }
        $name

        $apiversion = ""
        $goldversion = ""
        $apiBuilddate = ""

       add-content -path "\\store1\share\repo_report\whatsoncustbox.csv" -value $name
}